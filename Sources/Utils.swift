//
//  Utils.swift
//  promises
//
//  Created by Nikolay Petrov on 10/19/16.
//
//

import Foundation

extension DispatchQueue {
    static var background: DispatchQueue {
        return DispatchQueue.global(qos: .background)
    }
}
