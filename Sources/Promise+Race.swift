//
//  Promise+Race.swift
//  Promises
//
//  Created by Nikolay Petrov on 2/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

public enum PromiseRaceError: Error {
    case noPromisesFound
}

extension Promise {
    public static func race<V>(on queue: DispatchQueue = DispatchQueue.main, promises: [Promise<V>]) -> Promise<V> {
        guard !promises.isEmpty else {
            return Promise.make(withError: PromiseRaceError.noPromisesFound)
        }

        let result: DeferredPromise<V> = Promise.make()
        var shouldResolve = true

        for promise in promises {
            promise.then(on: queue) { value in
                if shouldResolve {
                    result.fulfill(value)
                    shouldResolve = false
                }
                }.recover(on: queue) { error in
                    if shouldResolve {
                        result.reject(error)
                        shouldResolve = false
                    }
            }
        }
        
        return result
    }

    public static func raceInBackground<V>(_ promises: [Promise<V>]) -> Promise<V> {
        return race(on: DispatchQueue.background, promises: promises)
    }
}
