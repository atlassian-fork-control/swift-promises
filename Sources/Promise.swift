//
//  Promise.swift
//  Promises
//
//  Created by Nikolay Petrov on 9/17/15.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

private enum PromiseValue<Value> {
    case fulfilled(Value)
    case rejected(Error)
}

private struct PromiseListener<Value> {
    let queue: DispatchQueue
    let callback: (PromiseValue<Value>) -> Void
    
    func notify(_ value: PromiseValue<Value>) {
        queue.async {
            self.callback(value)
        }
    }
}

private enum PromiseListeners<Value> {
    case pending([PromiseListener<Value>])
    case none
    
    mutating func append(_ listener: PromiseListener<Value>) {
        switch self {
        case let .pending(listeners):
            var newListeners = listeners
            newListeners.append(listener)
            self = PromiseListeners.pending(newListeners)
        case .none:
            self = PromiseListeners.none
        }
    }
}

public class Promise<Value> {
    private let internalQueue = DispatchQueue(label: "org.promises.Promise", attributes: [])
    private var value: PromiseValue<Value>?
    private var listeners: PromiseListeners<Value> = PromiseListeners.pending([])
    
    fileprivate init() {
    }
    
    @discardableResult
    fileprivate func resolve(_ resolveValue: PromiseValue<Value>) -> Self {
        internalQueue.async {
            precondition(self.value == nil, "Promise already resolved")
            self.value = resolveValue

            if case let .pending(listeners) = self.listeners {
                for listener in listeners {
                    listener.notify(resolveValue)
                }
            }
            self.listeners = PromiseListeners.none
        }
        return self
    }
    
    // MARK: listener support
    fileprivate func listen(_ queue: DispatchQueue, callback: @escaping (PromiseValue<Value>) -> Void) {
        let listener = PromiseListener<Value>(queue: queue, callback: callback)
        internalQueue.async {
            if let value = self.value {
                listener.notify(value)
            } else {
                self.listeners.append(listener)
            }
        }
    }
}

public class DeferredPromise<Value> : Promise<Value> {
    public override init() {
        super.init()
    }

    public func fulfill(_ value: Value) {
        resolve(PromiseValue.fulfilled(value))
    }
    
    public func reject(_ error: Error) {
        resolve(PromiseValue.rejected(error))
    }
}

extension Promise {
    private func combine<NewValue>(_ queue: DispatchQueue, fun: @escaping (DeferredPromise<NewValue>, PromiseValue<Value>) -> Void) -> Promise<NewValue> {
        let promise: DeferredPromise<NewValue> = Promise.make()
        listen(queue) { promiseValue in
            fun(promise, promiseValue)
        }
        return promise
    }
    
    @discardableResult
    public func then<NewValue>(on queue: DispatchQueue = DispatchQueue.main, block: @escaping (Value) throws -> NewValue) -> Promise<NewValue> {
        return combine(queue) { promise, promiseValue in
            switch promiseValue {
            case let .fulfilled(value):
                do {
                    let newValue = try block(value)
                    promise.fulfill(newValue)
                } catch let error {
                    promise.reject(error)
                }
            case let .rejected(error):
                promise.reject(error)
            }
        }
    }

    @discardableResult
    public func thenInBackground<NewValue>(_ block: @escaping (Value) throws -> NewValue) -> Promise<NewValue> {
        return then(on: DispatchQueue.background, block: block)
    }

    @discardableResult
    public func then<NewValue>(on queue: DispatchQueue = DispatchQueue.main, block: @escaping (Value) throws -> Promise<NewValue>) -> Promise<NewValue> {
        return combine(queue) { promise, promiseValue in
            switch promiseValue {
            case let .fulfilled(value):
                do {
                    let internalPromise = try block(value)
                    internalPromise.listen(queue) { (internalPromiseValue) in
                        switch internalPromiseValue {
                        case let .fulfilled(internalValue):
                            promise.fulfill(internalValue)
                        case let .rejected(error):
                            promise.reject(error)
                        }
                    }
                } catch let error {
                    promise.reject(error)
                }
            case let .rejected(error):
                promise.reject(error)
            }
        }
    }

    @discardableResult
    public func thenInBackground<NewValue>(_ block: @escaping (Value) throws -> Promise<NewValue>) -> Promise<NewValue> {
        return then(on: DispatchQueue.background, block: block)
    }

    @discardableResult
    public func recover(on queue: DispatchQueue = DispatchQueue.main, block: @escaping (Error) throws -> Void) -> Promise<Value> {
        return combine(queue) { promise, promiseValue in
            switch promiseValue {
            case let .fulfilled(value):
                promise.fulfill(value)
            case let .rejected(originalError):
                do {
                    try block(originalError)
                    promise.reject(originalError)
                } catch let error {
                    promise.reject(error)
                }
            }
        }
    }

    @discardableResult
    public func recoverInBackground(_ block: @escaping (Error) throws -> Void) -> Promise<Value> {
        return recover(on: DispatchQueue.background, block: block)
    }

    @discardableResult
    public func always(on queue: DispatchQueue = DispatchQueue.main, block: @escaping () -> Void) -> Promise<Value> {
        return combine(queue) { promise, promiseValue in
            block()
            switch promiseValue {
            case let .fulfilled(value):
                promise.fulfill(value)
            case let .rejected(error):
                promise.reject(error)
            }
        }
    }

    @discardableResult
    public func alwaysInBackground(_ block: @escaping () -> Void) -> Promise<Value> {
        return always(on: DispatchQueue.background, block: block)
    }

    public func delay(_ interval: TimeInterval) -> Promise<Value> {
        return combine(DispatchQueue.background) { promise, promiseValue in
            let wait = DispatchTime.now() + Double(Int64(interval * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.background.asyncAfter(deadline: wait) {
                promise.resolve(promiseValue)
            }
        }
    }
}
