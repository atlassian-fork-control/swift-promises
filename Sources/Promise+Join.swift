//
//  Promises+Join.swift
//  Promises
//
//  Created by Nikolay Petrov on 9/30/15.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

extension Promise {
    public static func join<V>(on queue: DispatchQueue = DispatchQueue.main, promises: [Promise<V>]) -> Promise<([V], [Error])> {
        guard !promises.isEmpty else {
            return Promise.make(with: ([], []))
        }

        var results: [V] = []
        var errors: [Error] = []

        let result: DeferredPromise<([V], [Error])> = Promise.make()

        var promisesLeft = promises.count
        for promise in promises {
            promise.then(on: queue) { value in
                results.append(value)
            }.recover(on: queue) { error in
                errors.append(error)
            }.always(on: queue) {
                promisesLeft -= 1
                if promisesLeft == 0 {
                    result.fulfill((results, errors))
                }
            }
        }
        
        return result
    }

    public static func joinInBackground<V>(_ promises: [Promise<V>]) -> Promise<([V], [Error])> {
        return join(on: DispatchQueue.background, promises: promises)
    }
}
